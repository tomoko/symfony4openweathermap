# Symfony4OpenWeatherMap

### Online DEMO
[weatherapp.goldsoft.pl](http://weatherapp.goldsoft.pl)


### Specyfikacja

1. Strona główna z wdrożoną mapą Google Maps na takiej zasadzie, aby po kliknięciu na dowolny obszar
następowała akcje:
a. Na podstawie wybranych koordynacji geograficznych następuje komunikacja z API
http://openweathermap.org i pobierana jest aktualna pogoda dla wybranego miejsca (temperatura,
zachmurzenie, wiatr, ogólna informacja „description”).
b. Wynik zapisywany jest do bazy (łącznie z wybranym obszarem lat/lng oraz nazwą wybranego
miasta) i czasem wyszukiwania.
c. Wyświetlenie powyższych informacji w odrębnym modalu, który jest tworzony w momencie, gdy
dane zostały zapisane. Całość powinna działać w tle (bez przeładowania). Podczas zapisywania
powinno być zabezpieczenie strony loaderem, informującym użytkownika, że trwa zapisywanie
2. Historia wyszukiwań – odrębna podstrona, na której będzie:
a. Lista ostatnich wyszukiwań pogody zawierająca wszystkie informacje, np. w formie tabelki z
ograniczeniem wyświetlania 10 rekordów per podstrona (paginacja).
b. Ogólne statystyki z wszystkich pomiarów:
− Minimalna, maksymalna oraz średnia temperatura.
− Najczęściej wyszukiwanie miasto.
− Ilość łącznych wyszukiwań. 
3. Projekt postawiony na frameworku Symfony 3/4.
